import React from 'react';
import styled from 'styled-components/macro';

import TaskBox from './containers/TaskBox';

export default function Layout() {
  return (
    <Page>
      <TaskBox />
    </Page>
  );
}

const Page = styled.div`
  min-height: 100vh;
  background-color: #9578FF;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  padding: 20px 10px 20px;
`;
