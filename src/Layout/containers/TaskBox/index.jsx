import React, { useState } from 'react';
import styled from 'styled-components/macro';
import axios from 'axios';
import useAxios from 'axios-hooks';

import TaskLineItem from './components/TaskLineItem';

export default function TaskBox() {
  const [valueInput, setValueInput] = useState('');
  const [{ data, loading, error }, refetch] = useAxios(
    process.env.REACT_APP_API,
  );

  const dataTasks = data;

  const addTask = (key = 'Enter') => {
    if (key !== 'Enter' || valueInput === '') return;
    const task = valueInput;
    axios.post(`${process.env.REACT_APP_API}`, { task, taskPerformed: false });
    refetch();
    setValueInput('');
  };

  if (loading) return <p>Loading ...</p>;
  if (error) return <p>Error</p>;

  return (
    <Box>
      <InputButtonWrapper>
        <Input
          placeholder="Enter name task"
          onChange={(e) => setValueInput(e.target.value)}
          onKeyDown={(e) => addTask(e.key)}
        />
        <Button
          onClick={() => addTask()}
        >
          Add
        </Button>
      </InputButtonWrapper>
      {dataTasks.map((item) => (
        <TaskLineItem
          key={item.id}
          id={item.id}
          taskInfo={item.task}
          performed={item.taskPerformed}
          refetch={refetch}
        />
      ))}
    </Box>
  );
}

const Box = styled.div`
  max-width: 400px;
  width: 100%;
  /* min-height: 100px; */
  background-color: rgba(255, 255, 255, 0.5);
  border-radius: 10px;
  box-shadow: 0px 10px 20px -5px rgba(0, 0, 0, 0.5);
  box-sizing: border-box;
  padding: 10px;
`;

const InputButtonWrapper = styled.div`
  display: flex;
`;

const Input = styled.input`
  flex-grow: 2;
  background-color: rgba(255, 255, 255, 0.5);
  border: 1px solid rgba(0, 0, 0, 0.2);
  margin-right: 10px;
  border-radius: 4px;
  height: 40px;
  box-sizing: border-box;
  padding: 0 10px;
  font-size: 1rem;
  :focus {
    background-color: rgba(255, 255, 255, 0.7);
    outline: none;
  }
`;

const Button = styled.button`
  flex-grow: 1;
  border-radius: 4px;
  border: none;
  transition: .05s linear;
  background-color: rgba(255, 255, 255, 0.5);
  :active {
    box-shadow: 0px 0px 5px 1px rgba(0, 0, 0, 0.5);
  };
  :focus {
    outline: none;
  }
`;
