import React, { useState } from 'react';
import styled from 'styled-components/macro';
import PropTypes from 'prop-types';
import axios from 'axios';

export default function TaskLineItem({
  taskInfo, refetch, id, performed,
}) {
  const [valueInput, setValueInput] = useState('');

  const patchTask = (key) => {
    if (key !== 'Enter') return;
    const task = valueInput;
    axios.patch(`${process.env.REACT_APP_API}/${id}`, { task });
    refetch();
  };

  const performedTask = (boolean) => {
    const taskPerformed = boolean;
    axios.patch(`${process.env.REACT_APP_API}/${id}`, { taskPerformed });
    refetch();
  };

  const deleteTask = () => {
    axios.delete(`${process.env.REACT_APP_API}/${id}`);
    refetch();
  };

  return (
    <Container>
      <CheckBox
        type="checkbox"
        defaultChecked={performed}
        onChange={(e) => performedTask(e.target.checked)}
      />
      <InputTask
        defaultValue={taskInfo}
        onChange={(e) => setValueInput(e.target.value)}
        onKeyDown={(e) => patchTask(e.key)}
        id={id}
      />
      <DeleteButton onClick={deleteTask}>
        &#215;
      </DeleteButton>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  box-sizing: border-box;
  padding: 10px 0;
  align-items: center;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`;

const InputTask = styled.input`
  background-color: transparent;
  border: none;
  outline: none;
  height: 20px;
  font-size: 1rem;
  width: 70%;
  :focus {
    border-bottom: 1px solid rgba(0, 0, 0, 0.2);
    box-sizing: border-box;
  }
`;

const DeleteButton = styled.button`
  min-width: 20px;
  min-height: 20px;
  color: #FFF;
  font-size: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #CC3D3D;
  border: none;
  outline: none;
  border-radius: 50%;
  padding: 0;
  margin-left: auto;
  margin-right: 10px;
  :active {
    box-shadow: 0px 0px 2px 1px rgba(0, 0, 0, 0.5);
  };
  :focus {
    border: none;
  }
`;

const CheckBox = styled.input`
  width: 20px;
  height: 20px;
  margin-right: 10px;
`;

TaskLineItem.propTypes = {
  taskInfo: PropTypes.string.isRequired,
  refetch: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  performed: PropTypes.bool.isRequired,
};
